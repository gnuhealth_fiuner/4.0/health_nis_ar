# This file is part health_nis_ar module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from decimal import Decimal
from unidecode import unidecode
from Levenshtein import ratio

from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.config import config


MIN_SCORE = config.get('health_domain', 'mpi_minimum_score', default=0.6)
try:
    if not isinstance(MIN_SCORE, float):
        MIN_SCORE = float(MIN_SCORE)
except Exception:
    MIN_SCORE = 0.6
if MIN_SCORE < 0.4 or MIN_SCORE > 1.0:
    MIN_SCORE = 0.6


def get_patients_by_dni(dni):
    pool = Pool()
    Party = pool.get('party.party')

    result = []
    parties = Party.search([
        ('is_person', '=', True),
        ('is_patient', '=', True),
        ('ref', '=', dni),
        ])
    for party in parties:
        result.append({
            'id': party.id,
            'name': party.name,
            'lastname': party.lastname,
            'ref': party.ref,
            'dob': party.dob,
            'gender': party.gender,
            'nis_state': party.nis_state,
            'score': Decimal('1.0')
            })

    return result


def get_mpi_patients(data, count=5):
    '''
    'data' includes: given_name, family_name, dni, gender, birth_date
    'count' is the max number of results
    '''
    pool = Pool()
    Party = pool.get('party.party')
    cursor = Transaction().connection.cursor()

    match_gender = data['gender'] == 'masculino' and 'm' or \
        data['gender'] == 'femenino' and 'f' or data['gender']
    # Prefiltro en base de datos (levenshtein/dmetaphone/unaccent)
    #   Apellido y Nombre: levenshtein <= 2
    #   o DNI/Ref y FechaNac: levenshtein <= 2
    #   Sexo: no se filtra
    cursor.execute(
        'SELECT p.id, p.name, p.lastname, p.ref, p.dob, p.gender, p.nis_state '
        'FROM "' + Party._table + '" AS p '
        'WHERE p.is_patient is TRUE AND p.is_person is TRUE '
        'AND ( (levenshtein(dmetaphone(unaccent(p.name)), '
        'dmetaphone(unaccent(\'' + data['given_name'] + '\'))) <= 2 '
        'AND levenshtein(dmetaphone(unaccent(p.lastname)), '
        'dmetaphone(unaccent(\'' + data['family_name'] + '\'))) <= 2) '
        'OR (levenshtein(p.ref, \'' + data['dni'] + '\') <= 2 '
        'AND levenshtein(to_char(p.dob, \'yyyy-mm-dd\'), '
        '\'' + str(data['birth_date']) + '\') <= 2) )')
    res = cursor.fetchall()
    if not res:
        return []

    # Calcular score
    # Puntaje maximo: 4.0
    #   -Gender y FechaNac: +0.5 cada uno, si coinciden
    #   -DNI, Apellido, Nombre: score Levenshtein [0.0 a 1.0]
    #   Score = acumulado / 4
    result = []
    for person in res:
        score = person[5] == match_gender and 0.5 or 0
        score += str(person[4]) == str(data['birth_date']) and 0.5 or 0
        score += ratio(person[3], data['dni'])
        score += ratio(
            unidecode(person[2]).upper(),
            unidecode(data['family_name']).upper())
        score += ratio(
            unidecode(person[1]).upper(),
            unidecode(data['given_name'].upper()))
        score = score / 4
        if score < MIN_SCORE:
            continue
        result.append({
            'id': person[0],
            'name': person[1],
            'lastname': person[2],
            'ref': person[3],
            'dob': person[4],
            'gender': person[5],
            'nis_state': person[6],
            'score': Decimal(str(round(score, 2)))
            })

    sorted_list = sorted(result, key=lambda d: d['score'], reverse=True)
    return sorted_list[:count]
