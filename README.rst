GNU Health / NIS: Nube de Información Sanitaria

Este módulo permite la integración de GNU Health con la Nube de Información
Sanitaria (NIS).
Por más información, ver el siguiente enlace:
https://www.argentina.gob.ar/salud/impulsa

Prerequisitos
-------------

 * Tryton 6.0 (http://www.tryton.org/)
 * Python 3.8 o superior (http://www.python.org/)
 * Librerías Python adicionales:
     -Levenshtein: https://pypi.org/project/Levenshtein/
     -Unidecode: https://pypi.org/project/Unidecode/
 * Extensiones de PostgreSQL:
     -fuzzystrmatch: incorpora 'levenshtein' y 'dmetaphone'
       Ejecutar: CREATE EXTENSION fuzzystrmatch;
     -unaccent: para remover diacríticos, como los acentos
       Ejecutar: CREATE EXTENSION unaccent;

Configuration
-------------

Este módulo funciona en conjunto con el servicio 'health_domain_service'.
Ver indicaciones en el módulo correspondiente.
Es necesario definir la 'url' del servicio y la misma 'service_key' en
el archivo trytond.conf, para asegurar la comunicación entre Tryton y
este servicio.
Por otro lado, se debe indicar la url de dominio asignada, utilizando
la etiqueta 'domain', por ejemplo:

    [health_domain]
    url=http://localhost:5000
    service_key = HQHTXuSD0AO9N0uE4UDxofGakq8Zee3DOZkIZVn0nMY=
    domain=https://conn2dummy.org.ar

Para generar una service_key, se puede ejecutar desde el intérprete de Python:

    >>> import os
    >>> import base64
    >>> print(base64.urlsafe_b64encode(os.urandom(32)).decode())

Opcionalmente, y bajo la misma etiqueta de [health_domain], se puede definir
también el ratio mínimo de coincidencia (o score) a partir del cual
se considera la semejanza de un paciente buscado en base de datos
(MPI - Master Patient Index):

    mpi_minimum_score=0.6

Los valores aceptados van de 0.4 a 1.0. El valor por defecto es 0.6.


Soporte
-------

Para más información, o si encuentra algún problema en el módulo,
favor de contactar a:

  Silix
  --------------
  website: http://www.silix.com.ar
  email: contacto@silix.com.ar

Licencia
--------

Ver LICENSE

Copyright
---------

Ver COPYRIGHT
