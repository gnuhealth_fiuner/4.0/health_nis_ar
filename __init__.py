# This file is part health_nis_ar module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import party
from . import patient
from . import patient_federator


def register():
    Pool.register(
        party.Party,
        patient.Patient,
        patient.PatientIdentifyStart,
        patient.PatientIdentifyMpiResultLine,
        patient.PatientIdentifyMpiResult,
        patient.PatientIdentifyNisInMpiResultLine,
        patient.PatientIdentifyNisInMpiResult,
        patient.PatientIdentifyFederatorResultLine,
        patient.PatientIdentifyFederatorResult,
        patient.PatientIdentifyMpiNewPatient,
        patient_federator.PatientCheckFederatorResultLine,
        patient_federator.PatientCheckFederatorResult,
        patient_federator.PatientCheckFederatorNotFound,
        patient_federator.PatientCheckFederatorSuccess,
        patient_federator.PatientCheckFederatorFailure,
        module='health_nis_ar', type_='model')
    Pool.register(
        patient.PatientIdentify,
        patient_federator.PatientCheckFederator,
        module='health_nis_ar', type_='wizard')
