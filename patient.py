# This file is part health_nis_ar module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import sys
from decimal import Decimal

from trytond.model import fields, ModelView
from trytond.model.exceptions import ValidationError
from trytond.pool import PoolMeta, Pool
from trytond.wizard import (Wizard, StateView, StateTransition, StateAction,
    Button)
from trytond.i18n import gettext
from .domain_service import DomainService
from .utils import get_patients_by_dni, get_mpi_patients


class Patient(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient'

    nis_state = fields.Function(fields.Selection([
        (None, ''),
        ('permanent', 'Permanent'),
        ('validated', 'Validated'),
        ('temporary', 'Temporary'),
        ('historic', 'Historic'),
        ('telephonic', 'Telephonic'),
        ('rejected', 'Rejected'),
        ], 'State'), 'get_nis_state', searcher='search_nis_state')

    def get_nis_state(self, name):
        return self.name.nis_state

    @classmethod
    def search_nis_state(cls, name, clause):
        res = []
        res.append(('name.nis_state', clause[1], clause[2]))
        return res


class PatientIdentifyStart(ModelView):
    'Patient Identify Start'
    __name__ = 'gnuhealth.patient.identify.start'

    nro_documento = fields.Char('ID number', required=True)


class PatientIdentifyMpiResultLine(ModelView):
    'Patient Identify MPI Result Line'
    __name__ = 'gnuhealth.patient.identify.mpi_result.line'

    result = fields.Many2One('gnuhealth.patient.identify.mpi_result',
        'Result', required=True, select=True)
    id = fields.Numeric('ID', readonly=True)
    lastname = fields.Char('Lastname', readonly=True)
    name = fields.Char('Name', readonly=True)
    ref = fields.Char('DNI', readonly=True)
    dob = fields.Date('Birth date', readonly=True)
    gender = fields.Char('Gender', readonly=True)
    nis_state = fields.Selection([
        (None, ''),
        ('permanent', 'Permanent'),
        ('validated', 'Validated'),
        ('temporary', 'Temporary'),
        ('historic', 'Historic'),
        ('telephonic', 'Telephonic'),
        ('rejected', 'Rejected'),
        ], 'State', sort=False, readonly=True)
    score = fields.Numeric('Score', digits=(3, 1), readonly=True)
    selected = fields.Boolean('Selected', help='Check to select')


class PatientIdentifyMpiResult(ModelView):
    'Patient Identify MPI Result'
    __name__ = 'gnuhealth.patient.identify.mpi_result'

    nro_documento = fields.Char('ID number', readonly=True)
    selected_id = fields.Numeric('Selected id')
    results = fields.One2Many(
        'gnuhealth.patient.identify.mpi_result.line',
        'result', 'Results in database')

    @fields.depends('selected_id', 'results')
    def on_change_results(self):
        # only one 'selected' at a time
        previous = self.selected_id
        self.selected_id = None
        for r in self.results:
            if r.selected:
                if r.id == previous:
                    r.selected = False
                else:
                    self.selected_id = r.id


class PatientIdentifyNisInMpiResultLine(ModelView):
    'Patient Identify NIS in MPI Result Line'
    __name__ = 'gnuhealth.patient.identify.nis_in_mpi_result.line'

    result = fields.Many2One('gnuhealth.patient.identify.nis_in_mpi_result',
        'Result', required=True, select=True)
    id = fields.Numeric('ID', readonly=True)
    lastname = fields.Char('Lastname', readonly=True)
    name = fields.Char('Name', readonly=True)
    ref = fields.Char('DNI', readonly=True)
    dob = fields.Date('Birth date', readonly=True)
    gender = fields.Char('Gender', readonly=True)
    nis_state = fields.Selection([
        (None, ''),
        ('permanent', 'Permanent'),
        ('validated', 'Validated'),
        ('temporary', 'Temporary'),
        ('historic', 'Historic'),
        ('telephonic', 'Telephonic'),
        ('rejected', 'Rejected'),
        ], 'State', sort=False, readonly=True)
    score = fields.Numeric('Score', digits=(3, 1), readonly=True)
    selected = fields.Boolean('Selected', help='Check to select')


class PatientIdentifyNisInMpiResult(ModelView):
    'Patient Identify NIS in MPI Result'
    __name__ = 'gnuhealth.patient.identify.nis_in_mpi_result'

    apellido = fields.Char('Lastname', readonly=True)
    nombre = fields.Char('Name', readonly=True)
    nro_documento = fields.Char('ID number', readonly=True)
    sexo = fields.Char('Gender', readonly=True)
    fecha_nacimiento = fields.Date('Birth date', readonly=True)
    selected_id = fields.Numeric('Selected id')
    results = fields.One2Many(
        'gnuhealth.patient.identify.nis_in_mpi_result.line',
        'result', 'Results in database')

    @fields.depends('selected_id', 'results')
    def on_change_results(self):
        # only one 'selected' at a time
        previous = self.selected_id
        self.selected_id = None
        for r in self.results:
            if r.selected:
                if r.id == previous:
                    r.selected = False
                else:
                    self.selected_id = r.id


class PatientIdentifyFederatorResultLine(ModelView):
    'Patient Identify Federator Result Line'
    __name__ = 'gnuhealth.patient.identify.federator_result.line'

    result = fields.Many2One('gnuhealth.patient.identify.federator_result',
        'Result', required=True, select=True)
    resource_id = fields.Char('Resource ID', readonly=True)
    family_name = fields.Char('Family name', readonly=True)
    given_name = fields.Char('Given name', readonly=True)
    dni = fields.Char('DNI', readonly=True)
    birth_date = fields.Date('Birth date', readonly=True)
    gender = fields.Char('Gender', readonly=True)
    active = fields.Boolean('Active', readonly=True)
    score = fields.Numeric('Score', digits=(3, 1), readonly=True)
    selected = fields.Boolean('Selected', help='Check to select')


class PatientIdentifyFederatorResult(ModelView):
    'Patient Identify Federator Result'
    __name__ = 'gnuhealth.patient.identify.federator_result'

    nro_documento = fields.Char('ID number', readonly=True)
    selected_id = fields.Numeric('Selected id')
    results = fields.One2Many(
        'gnuhealth.patient.identify.federator_result.line',
        'result', 'Results in Federator')

    @fields.depends('selected_id', 'results')
    def on_change_results(self):
        # only one 'selected' at a time
        previous = self.selected_id
        self.selected_id = None
        for r in self.results:
            if r.selected:
                if r.id == previous:
                    r.selected = False
                else:
                    self.selected_id = r.id


class PatientIdentifyMpiNewPatient(ModelView):
    'Patient Identify MPI New Patient'
    __name__ = 'gnuhealth.patient.identify.mpi_new_patient'

    name = fields.Char('Name', required=True)
    lastname = fields.Char('Lastname', required=True)
    ref = fields.Char('ID number', required=True)
    gender = fields.Selection([
        (None, ''),
        ('m', 'Male'),
        ('f', 'Female'),
        ('nb', 'Non-binary'),
        ('other', 'Other'),
        ('nd', 'Non disclosed'),
        ('u', 'Unknown')
        ], 'Gender', required=True)
    dob = fields.Date('Birth date', required=True)


class PatientIdentify(Wizard):
    'Patient Identify'
    __name__ = 'gnuhealth.patient.identify'

    start_state = 'start'
    start = StateView('gnuhealth.patient.identify.start',
        'health_nis_ar.view_patient_identify_start', [
            Button('Check Federator', 'check_federator', 'tryton-search',
                default=True),
            Button('Check in database', 'check_mpi', 'tryton-search'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    check_federator = StateTransition()
    federator_result = StateView('gnuhealth.patient.identify.federator_result',
        'health_nis_ar.view_patient_identify_federator_result', [
            Button('Check in database', 'check_nis_in_mpi', 'tryton-ok',
                default=True),
            Button('Search again', 'start', 'tryton-ok'),
            Button('Close', 'end', 'tryton-close'),
            ])
    check_mpi = StateTransition()
    mpi_result = StateView('gnuhealth.patient.identify.mpi_result',
        'health_nis_ar.view_patient_identify_mpi_result', [
            Button('Create patient', 'form_new_patient', 'tryton-ok',
                default=True),
            Button('Search again', 'start', 'tryton-back'),
            Button('Close', 'end', 'tryton-close'),
            ])
    check_nis_in_mpi = StateTransition()
    nis_in_mpi_result = StateView(
        'gnuhealth.patient.identify.nis_in_mpi_result',
        'health_nis_ar.view_patient_identify_nis_in_mpi_result', [
            Button('Create new patient', 'check_create_patient', 'tryton-ok',
                default=True),
            Button('Update patient', 'check_update_patient', 'tryton-ok'),
            Button('Search again', 'start', 'tryton-back'),
            Button('Close', 'end', 'tryton-close'),
            ])
    form_new_patient = StateView('gnuhealth.patient.identify.mpi_new_patient',
        'health_nis_ar.view_patient_identify_mpi_new_patient', [
            Button('Create', 'mpi_create_patient', 'tryton-ok',
                default=True),
            Button('Cancel', 'end', 'tryton-close'),
            ])
    check_create_patient = StateTransition()
    create_patient = StateAction('health.action_gnuhealth_patient_view')
    check_update_patient = StateTransition()
    update_patient = StateAction('health.action_gnuhealth_patient_view')
    mpi_create_patient = StateAction('health.action_gnuhealth_patient_view')
    records_list = []
    selected_patient = None
    mpi_patients = []

    def transition_check_federator(self):
        nro_doc = self.start.nro_documento.replace('.', '').replace(' ', '')
        self.start.nro_documento = nro_doc
        result_data = DomainService.patient_by_dni(nro_doc)
        records_list = []
        for res in result_data:
            gender = None
            if res['gender'] and res['gender'] == 'female':
                gender = 'femenino'
            elif res['gender'] and res['gender'] == 'male':
                gender = 'masculino'
            data = {
                'resource_id': res['id'],
                'family_name': res['name'][0]['family']
                    if res['name'] else None,
                'given_name': res['name'][0]['given']
                    if res['name'] else None,
                'active': res['active'],
                'birth_date': res['birthDate'],
                'gender': gender,
                'score': Decimal('100.0'),
                'selected': False
                }
            dni = [i for i in res['identifier']
                if i['system'] == 'http://www.renaper.gob.ar/dni']
            if dni:
                data['dni'] = dni[0]['value']
            records_list.append(data)

        self.records_list = records_list
        return 'federator_result'

    def default_federator_result(self, fields):
        return {
            'nro_documento': self.start.nro_documento,
            'selected_id': None,
            'results': self.records_list
            }

    def transition_check_mpi(self):
        nro_doc = self.start.nro_documento.replace('.', '').replace(' ', '')
        self.start.nro_documento = nro_doc
        self.mpi_patients = get_patients_by_dni(nro_doc)
        for p in self.mpi_patients:
            p['gender'] = p['gender'] == 'm' and 'Masculino' or \
                p['gender'] == 'f' and 'Femenino' or p['gender']
            p['selected'] = False

        return 'mpi_result'

    def default_mpi_result(self, fields):
        return {
            'nro_documento': self.start.nro_documento,
            'selected_id': None,
            'results': self.mpi_patients,
            }

    def transition_check_nis_in_mpi(self):
        if not self.federator_result.results:
            return 'check_mpi'
        selected = [p for p in self.federator_result.results
            if p.selected is True]
        if not selected and len(self.federator_result.results) == 1:
            selected = self.federator_result.results
        if not selected:
            raise ValidationError(
                gettext('health_nis_ar.msg_must_select_patient'))
            return 'federator_result'
        nis_patient = selected[0]
        data = {
            'resource_id': nis_patient.resource_id,
            'family_name': nis_patient.family_name,
            'given_name': nis_patient.given_name,
            'dni': nis_patient.dni,
            'active': nis_patient.active,
            'birth_date': nis_patient.birth_date,
            'gender': nis_patient.gender,
            'score': nis_patient.score,
            'selected': nis_patient.selected
            }

        self.selected_patient = data
        self.mpi_patients = get_mpi_patients(data)
        for p in self.mpi_patients:
            p['gender'] = p['gender'] == 'm' and 'Masculino' or \
                p['gender'] == 'f' and 'Femenino' or p['gender']
            p['score'] = p['score'] * 100
            p['selected'] = False

        return 'nis_in_mpi_result'

    def default_nis_in_mpi_result(self, fields):
        return {
            'apellido': self.selected_patient['family_name'],
            'nombre': self.selected_patient['given_name'],
            'nro_documento': self.selected_patient['dni'],
            'sexo': self.selected_patient['gender'],
            'fecha_nacimiento': self.selected_patient['birth_date'],
            'selected_id': None,
            'results': self.mpi_patients,
            }

    def transition_check_create_patient(self):
        pool = Pool()
        Party = pool.get('party.party')

        for r in self.nis_in_mpi_result.results:
            if r.score == Decimal('100.0'):
                raise ValidationError(
                    gettext('health_nis_ar.msg_mpi_patient_match'))
                return 'mpi_result'
        parties = Party.search([
            ('is_person', '=', True),
            ('is_patient', '=', True),
            ('ref', '=', self.nis_in_mpi_result.nro_documento),
            ])
        if parties:
            raise ValidationError(
                gettext('health_nis_ar.msg_patient_already_exists'))
            return 'mpi_result'
        return 'create_patient'

    def do_create_patient(self, action):
        pool = Pool()
        Party = pool.get('party.party')
        Patient = pool.get('gnuhealth.patient')

        gender = None
        if self.nis_in_mpi_result.sexo == 'femenino':
            gender = 'f'
        elif self.nis_in_mpi_result.sexo == 'masculino':
            gender = 'm'

        identifier_data = {
            'type': 'ar_dni',
            'code': self.nis_in_mpi_result.nro_documento,
            }
        party_data = {
            'name': self.nis_in_mpi_result.nombre,
            'lastname': self.nis_in_mpi_result.apellido,
            'ref': self.nis_in_mpi_result.nro_documento,
            'identifiers': [('create', [identifier_data])],
            'dob': self.nis_in_mpi_result.fecha_nacimiento,
            'gender': gender,
            'is_person': True,
            'is_patient': True,
            'nis_state': 'permanent',
            'nis_local_id': Party.generate_nis_local_id(),
            'federation_account': self.nis_in_mpi_result.nro_documento,
            }
        # 'party_ar' module requires vat_number definition
        if 'trytond.modules.party_ar' in sys.modules:
            party_data['iva_condition'] = 'consumidor_final'

        party = Party.create([party_data])
        patient = []
        if party:
            patient = Patient.create([{
                'name': party[0],
                }])

        # Try to federate the new patient
        for p in patient:
            patient_data = {
                'apellido': p.name.lastname,
                'nombre': p.name.name,
                'nro_documento': p.name.ref,
                'nis_local_id': p.name.nis_local_id,
                'sexo': p.name.gender == 'm' and 'male' or
                    p.name.gender == 'f' and 'female',
                'fecha_nacimiento': p.name.dob or None,
                }

            result_data = DomainService.patient_federate(patient_data)
            if result_data:
                status_code = result_data[0]
                reason = result_data[1]
                if status_code == 201:
                    party[0].nis_state = 'validated'
                    Party.save(party)
                    print('Paciente federado')
                else:
                    print('Error: %s' % reason)
            else:
                print('Error al intentar federar el paciente')

        data = {'res_id': [p.id for p in patient]}
        if len(patient) == 1:
            action['views'].reverse()
        return action, data

    def transition_check_update_patient(self):
        selected = [p for p in self.nis_in_mpi_result.results
            if p.selected is True]
        if not selected and len(self.nis_in_mpi_result.results) == 1:
            selected = self.nis_in_mpi_result.results
        if not selected:
            raise ValidationError(
                gettext('health_nis_ar.msg_must_select_patient'))
            return 'nis_in_mpi_result'
        self.selected_patient = {
            'id': selected[0].id,
            'name': selected[0].name,
            'lastname': selected[0].lastname,
            'ref': selected[0].ref,
            'dob': selected[0].dob,
            'gender': selected[0].gender,
            }
        return 'update_patient'

    def do_update_patient(self, action):
        pool = Pool()
        Party = pool.get('party.party')
        Patient = pool.get('gnuhealth.patient')

        gender = None
        if self.nis_in_mpi_result.sexo == 'femenino':
            gender = 'f'
        elif self.nis_in_mpi_result.sexo == 'masculino':
            gender = 'm'

        party, = Party.browse([self.selected_patient['id']])
        party.name = self.nis_in_mpi_result.nombre
        party.lastname = self.nis_in_mpi_result.apellido
        party.ref = self.nis_in_mpi_result.nro_documento
        party.dob = self.nis_in_mpi_result.fecha_nacimiento
        party.gender = gender
        party.nis_state = 'validated'
        Party.save([party])

        patient = Patient.search([
            ('name', '=', party.id)
            ])

        data = {'res_id': [p.id for p in patient]}
        if len(patient) == 1:
            action['views'].reverse()
        return action, data

    def do_mpi_create_patient(self, action):
        pool = Pool()
        Party = pool.get('party.party')
        Patient = pool.get('gnuhealth.patient')

        identifier_data = {
            'type': 'ar_dni',
            'code': self.form_new_patient.ref,
            }
        party_data = {
            'name': self.form_new_patient.name,
            'lastname': self.form_new_patient.lastname,
            'ref': self.form_new_patient.ref,
            'identifiers': [('create', [identifier_data])],
            'dob': self.form_new_patient.dob,
            'gender': self.form_new_patient.gender,
            'is_person': True,
            'is_patient': True,
            'nis_state': 'permanent',
            'nis_local_id': Party.generate_nis_local_id(),
            'federation_account': self.form_new_patient.ref,
            }
        # 'party_ar' module requires vat_number definition
        if 'trytond.modules.party_ar' in sys.modules:
            party_data['iva_condition'] = 'consumidor_final'

        party = Party.create([party_data])
        patient = []
        if party:
            patient = Patient.create([{
                'name': party[0],
                }])

        data = {'res_id': [p.id for p in patient]}
        if len(patient) == 1:
            action['views'].reverse()
        return action, data
