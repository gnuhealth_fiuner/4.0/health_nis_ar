# This file is part health_nis_ar module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import requests
import json
from cryptography.fernet import Fernet
from fhir.resources.bundle import Bundle
from fhir.resources.patient import Patient
from fhir.resources.parameters import Parameters

from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.config import config


# HL7/FHIR definitions
URL_FATHERS_FAMILY_NAME = \
    'http://hl7.org/fhir/StructureDefinition/humanname-fathers-family'


class DomainService(object):
    'Domain Service'

    url = config.get('health_domain', 'url', default='localhost:5000')
    service_key = config.get('health_domain', 'service_key', default=None)
    domain_name = config.get('health_domain', 'domain', default=None)

    @classmethod
    def patient_by_dni(cls, id_number):
        User = Pool().get('res.user')

        user = User(Transaction().user)
        f = Fernet(cls.service_key)
        user_login = user.login
        user_token = f.encrypt(user_login.encode()).decode()

        headers = {
            'Content-Type': 'application/json',
        }
        data = {
            'user': user_login,
            'token': user_token,
            'id_number': id_number
            }
        try:
            res_data = []
            request_url = cls.url + '/get_by_dni'
            response = requests.post(request_url, headers=headers,
                data=json.dumps(data))
            if response.status_code == 200:
                parsed = json.loads(response.json())
                if 'resourceType' in parsed and \
                        parsed['resourceType'] == 'Bundle':
                    bundle = Bundle.parse_obj(parsed)
                    if bundle.total < 1:
                        return res_data
                    for e in bundle.entry:
                        res = e.resource
                        if res.resource_type == 'Patient':
                            res_data.append(cls.get_patient(res))
            else:
                print("ERROR: ", response.status_code)

            return res_data
        except Exception:
            return None

    def get_patient(resource):
        res = {
            'id': resource.id,
            'name': [],
            'identifier': [],
            'active': resource.active,
            'birthDate': resource.birthDate,
            'gender': resource.gender,
            }
        for name in resource.name:
            extension = []
            if name.family__ext:
                for ext in name.family__ext.extension:
                    extension.append({
                        'url': ext.url,
                        'valueString': ext.valueString
                        })
                res['name'].append({
                    'family': name.family,
                    'given': ' '.join(name.given),
                    'extension': extension
                    })
        for ident in resource.identifier:
            res['identifier'].append({
                'system': ident.system,
                'use': ident.use,
                'value': ident.value
                })
        return res

    @classmethod
    def patient_match(cls, data):
        User = Pool().get('res.user')

        user = User(Transaction().user)
        f = Fernet(cls.service_key)
        user_login = user.login
        user_token = f.encrypt(user_login.encode()).decode()

        patient = Patient()
        patient.identifier = [{
            'use': 'usual',
            'system': 'http://www.renaper.gob.ar/dni',
            'value': data['nro_documento']
            }]
        patient.name = [{
            '_family': {
                'extension': [{
                    'url': URL_FATHERS_FAMILY_NAME,
                    'valueString': data['apellido']
                    }]
                },
            'given': data['nombre'].split()
            }]
        patient.gender = data['sexo'] or None
        patient.birthDate = data['fecha_nacimiento'] or None

        parameters = Parameters()
        parameters.id = 'mymatch'
        parameters.parameter = [{
            'name': 'resource',
            'resource': json.loads(patient.json())
            }, {
            'name': 'count',
            'valueInteger': 5
            }]

        headers = {
            'Content-Type': 'application/json',
        }
        payload = {
            'user': user_login,
            'token': user_token,
            'data': parameters.json(),
            }

        try:
            res_data = {}
            request_url = cls.url + '/match'
            response = requests.post(request_url, headers=headers,
                data=json.dumps(payload))
            if response.status_code == 200:
                parsed = json.loads(response.json())
                if 'resourceType' in parsed and \
                        parsed['resourceType'] == 'Bundle':
                    bundle = Bundle.parse_obj(parsed)
                    res_data['total'] = bundle.total
                    res_data['patients'] = []
                    if bundle.total < 1:
                        return res_data
                    for e in bundle.entry:
                        res = e.resource
                        if res.resource_type == 'Patient':
                            patient = {
                                'id': res.id,
                                'name': [],
                                'identifier': [],
                                'active': res.active,
                                'birthDate': res.birthDate,
                                'gender': res.gender,
                                }
                            for name in res.name:
                                patient['name'].append({
                                    'family': name.family,
                                    'given': ' '.join(name.given),
                                    })
                            for ident in res.identifier:
                                patient['identifier'].append({
                                    'system': ident.system,
                                    'use': ident.use,
                                    'value': ident.value
                                    })
                            patient['score'] = e.search.score
                            res_data['patients'].append(patient)
            else:
                print('ERROR: ', response.status_code)

            return res_data
        except Exception:
            print('Exception: error')
            return None

    @classmethod
    def patient_federate(cls, data):
        User = Pool().get('res.user')

        user = User(Transaction().user)
        f = Fernet(cls.service_key)
        user_login = user.login
        user_token = f.encrypt(user_login.encode()).decode()

        patient = Patient()
        patient.identifier = [
            {'system': cls.domain_name,
            'value': data['nis_local_id']},
            {'system': 'http://www.renaper.gob.ar/dni',
            'value': data['nro_documento']},
            ]
        patient.name = [{
            'use': 'official',
            'text': data['nombre'] + ' ' + data['apellido'],
            'family': data['apellido'],
            '_family': {
                'extension': [{
                    'url': URL_FATHERS_FAMILY_NAME,
                    'valueString': data['apellido']
                    }]
                },
            'given': data['nombre'].split()
            }]
        patient.gender = data['sexo'] or None
        patient.birthDate = data['fecha_nacimiento'] or None

        headers = {
            'Content-Type': 'application/json',
        }
        payload = {
            'user': user_login,
            'token': user_token,
            'data': patient.json(),
            }

        try:
            request_url = cls.url + '/federate'
            response = requests.post(request_url, headers=headers,
                data=json.dumps(payload))
            if response.status_code == 200:
                return list(response.json())
            else:
                print('ERROR: ', response.status_code)
                return None

        except Exception:
            print('Exception: error')
            return None
