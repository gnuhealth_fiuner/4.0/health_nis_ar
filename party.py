# This file is part health_nis_ar module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import random
import string

from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Not, Bool, Equal


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    nis_state = fields.Selection([
        (None, ''),
        ('permanent', 'Permanent'),
        ('validated', 'Validated'),
        ('temporary', 'Temporary'),
        ('historic', 'Historic'),
        ('telephonic', 'Telephonic'),
        ('rejected', 'Rejected'),
        ], 'State', sort=False, readonly=True,
        states={'invisible': Not(Bool(Eval('is_person')))},
        help='Shows Federation state')
    nis_local_id = fields.Char('Local ID',
        readonly=True, help='Local ID',
        states={'invisible': Not(Bool(Eval('is_person')))})

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        cls.name.states['readonly'] = Equal(Eval('nis_state'), 'validated')
        cls.lastname.states['readonly'] = Equal(Eval('nis_state'), 'validated')
        cls.ref.states['readonly'] = Equal(Eval('nis_state'), 'validated')

    @staticmethod
    def default_nis_state():
        return 'temporary'

    @classmethod
    def generate_nis_local_id(cls):
        puid = ''
        for x in range(9):
            puid = puid + random.choice(string.digits)
        return puid

    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('nis_local_id') and values.get('is_person'):
                values['nis_local_id'] = cls.generate_nis_local_id()
            if values.get('nis_local_id') == '':
                values['nis_local_id'] = None

        parties = super(Party, cls).create(vlist)
        return parties
