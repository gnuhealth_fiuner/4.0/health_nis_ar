# This file is part health_nis_ar module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.model import fields, ModelView
from trytond.model.exceptions import ValidationError
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.i18n import gettext
from .domain_service import DomainService


class PatientCheckFederatorResultLine(ModelView):
    'Patient Check Federator Result Line'
    __name__ = 'gnuhealth.patient.check_federator.result.line'

    result = fields.Many2One('gnuhealth.patient.check_federator.result',
        'Result', required=True, select=True)
    resource_id = fields.Char('Resource ID', readonly=True)
    family_name = fields.Char('Family name', readonly=True)
    given_name = fields.Char('Given name', readonly=True)
    dni = fields.Char('DNI', readonly=True)
    birth_date = fields.Date('Birth date', readonly=True)
    gender = fields.Char('Gender', readonly=True)
    active = fields.Boolean('Active', readonly=True)
    score = fields.Numeric('Score', digits=(3, 1), readonly=True)
    selected = fields.Boolean('Selected', help='Check to select')


class PatientCheckFederatorResult(ModelView):
    'Patient Check Federator Result'
    __name__ = 'gnuhealth.patient.check_federator.result'

    apellido = fields.Char('Lastname', readonly=True)
    nombre = fields.Char('Name', readonly=True)
    nro_documento = fields.Char('ID number', readonly=True)
    sexo = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        ], 'Gender', readonly=True)
    fecha_nacimiento = fields.Date('Birth date', readonly=True)
    nis_state = fields.Selection([
        (None, ''),
        ('permanent', 'Permanent'),
        ('validated', 'Validated'),
        ('temporary', 'Temporary'),
        ('historic', 'Historic'),
        ('telephonic', 'Telephonic'),
        ('rejected', 'Rejected'),
        ], 'State', sort=False, readonly=True)
    selected_id = fields.Numeric('Selected id')
    results = fields.One2Many(
        'gnuhealth.patient.check_federator.result.line',
        'result', 'Results in Federator')

    @fields.depends('selected_id', 'results')
    def on_change_results(self):
        # only one 'selected' at a time
        previous = self.selected_id
        self.selected_id = None
        for r in self.results:
            if r.selected:
                if r.id == previous:
                    r.selected = False
                else:
                    self.selected_id = r.id


class PatientCheckFederatorNotFound(ModelView):
    'Patient Check Federator Not Found'
    __name__ = 'gnuhealth.patient.check_federator.not_found'


class PatientCheckFederatorSuccess(ModelView):
    'Patient Check Federator Success'
    __name__ = 'gnuhealth.patient.check_federator.success'


class PatientCheckFederatorFailure(ModelView):
    'Patient Check Federator Failure'
    __name__ = 'gnuhealth.patient.check_federator.failure'

    message = fields.Char('Error message', readonly=True)


class PatientCheckFederator(Wizard):
    'Patient Check Federator'
    __name__ = 'gnuhealth.patient.check_federator'
    start_state = 'check'

    check = StateTransition()
    result = StateView('gnuhealth.patient.check_federator.result',
        'health_nis_ar.patient_check_federator_result', [
            Button('Update from Federator', 'update_from_federator',
                'tryton-ok', True),
            Button('Close', 'end', 'tryton-close'),
            ])
    not_found = StateView('gnuhealth.patient.check_federator.not_found',
        'health_nis_ar.patient_check_federator_not_found', [
            Button('Federate', 'federate', 'tryton-ok', True),
            Button('Close', 'end', 'tryton-close'),
            ])
    federate = StateTransition()
    update_from_federator = StateTransition()
    federator_success = StateView('gnuhealth.patient.check_federator.success',
        'health_nis_ar.patient_check_federator_success', [
            Button('Close', 'end', 'tryton-close', True),
            ])
    federator_failure = StateView('gnuhealth.patient.check_federator.failure',
        'health_nis_ar.patient_check_federator_failure', [
            Button('Close', 'end', 'tryton-close', True),
            ])
    result_list = []
    message = None

    def transition_check(self):
        if not self.records:
            raise ValidationError(
                gettext('health_nis_ar.msg_no_patient_selected'))
        if len(self.records) != 1:
            raise ValidationError(
                gettext('health_nis_ar.msg_too_many_patients_selected'))

        patient = self.records[0]
        data = {
            'apellido': patient.name.lastname,
            'nombre': patient.name.name,
            'nro_documento': patient.name.ref,
            'sexo': patient.name.gender == 'm' and 'male' or
                patient.name.gender == 'f' and 'female',
            'fecha_nacimiento': patient.name.dob or None,
            }

        result_data = DomainService.patient_match(data)
        if not result_data:
            raise ValidationError(gettext('health_nis_ar.msg_error_ws'))
        if result_data['total'] == 0:
            return 'not_found'
        result_list = []
        for res in result_data['patients']:
            gender = None
            if res['gender'] and res['gender'] == 'female':
                gender = 'femenino'
            elif res['gender'] and res['gender'] == 'male':
                gender = 'masculino'
            data = {
                'resource_id': res['id'],
                'family_name': res['name'][0]['family']
                    if res['name'] else None,
                'given_name': res['name'][0]['given']
                    if res['name'] else None,
                'active': res['active'],
                'birth_date': res['birthDate'],
                'gender': gender,
                'score': res['score'] * 100,
                'selected': False
                }
            dni = [i for i in res['identifier']
                if i['system'] == 'http://www.renaper.gob.ar/dni']
            if dni:
                data['dni'] = dni[0]['value']
            result_list.append(data)

        self.result_list = result_list
        return 'result'

    def default_result(self, fields):
        patient = self.records[0]
        return {
            'apellido': patient.name.lastname,
            'nombre': patient.name.name,
            'nro_documento': patient.name.ref,
            'sexo': patient.name.gender,
            'fecha_nacimiento': patient.name.dob,
            'nis_state': patient.name.nis_state,
            'selected_id': None,
            'results': self.result_list
            }

    def transition_federate(self):
        patient = self.records[0]
        data = {
            'apellido': patient.name.lastname,
            'nombre': patient.name.name,
            'nro_documento': patient.name.ref,
            'nis_local_id': patient.name.nis_local_id,
            'sexo': patient.name.gender == 'm' and 'male' or
                patient.name.gender == 'f' and 'female',
            'fecha_nacimiento': patient.name.dob or None,
            }

        result_data = DomainService.patient_federate(data)
        if not result_data:
            raise ValidationError(gettext('health_nis_ar.msg_error_ws'))
        status_code = result_data[0]
        reason = result_data[1]
        if status_code == 201:
            message = 'Paciente federado'
            self.message = message
            return 'federator_success'
        else:
            message = 'Error: %s' % reason
            self.message = message
            return 'federator_failure'

    def default_federator_failure(self, fields):
        return {'message': self.message}

    def transition_update_from_federator(self):
        pool = Pool()
        Party = pool.get('party.party')

        selected = [p for p in self.result.results
            if p.selected is True]
        if not selected and len(self.result.results) == 1:
            selected = self.result.results
        if not selected:
            raise ValidationError(
                gettext('health_nis_ar.msg_must_select_patient'))
            return 'result'

        patient = self.records[0]
        gender = None
        if selected[0].gender == 'femenino':
            gender = 'f'
        elif selected[0].gender == 'masculino':
            gender = 'm'

        party, = Party.browse([patient.name.id])
        party.name = selected[0].given_name
        party.lastname = selected[0].family_name
        party.ref = selected[0].dni
        party.dob = selected[0].birth_date
        party.gender = gender
        party.nis_state = 'validated'
        Party.save([party])

        return 'end'
